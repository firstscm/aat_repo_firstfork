<?
	include("connDB.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>เพิ่มรายการกาแฟ</title>
</head>

<body>
    <form action="addNewCoffeeRecepi_DB.php" method="post" name="addnewcoffee">
        <table border="0">
          <tr>
            <th scope="row" colspan="5"><strong>เพิ่มรายการควย</strong></th>
          </tr>
          <tr>
            <th scope="row" colspan="5">&nbsp;</th>
          </tr>
          <tr>
            <th scope="row">
                ชื่อรายการ<br />
                <em>(ภาษาไทย)</em>
            </th>
            <td><input name="txtCoffeeNameTH" type="text" size="40%" /></td>
            
            <th scope="row">
                ชื่อรายการ<br />
                <em>(ภาษาอังกฤษ)</em>
            </th>
            <td colspan="2"><input name="txtCoffeeNameEN" type="text" size="40%" /></td>
          </tr>
          
          
          <tr>
          </tr>
          <tr>
          </tr>
          <tr>
                    <th scope="row">ประเภท</th>
                    <td colspan="5">
                    <label for="coffeeType"></label>
                        <select name="coffeeType" id="coffeeType">
                          
                                <?
                                    $query = "select ctid,ctname,ctprice from coffeetype";
                                    $result = mysql_query($query) or die('Query failed: ' . mysql_error());
                                        while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
                                            $ctid = $line["ctid"];
                                            $ctname = $line["ctname"];
                                            $ctprice = $line["ctprice"];
                                ?>
                                    <option value="<?=$ctid?>">
                                        <?
                                            echo "$ctname (+$ctprice บาท)";
                                        ?>
                                    </option>
                              <?
                                        }
                              ?>
                        </select>
                    </td>
                  </tr>
         	
            
            
                  <tr>
                    <th scope="row">ส่วนผสม</th>
                    <td>
                        <label for="ingredient"></label>
                          <select name="ingredient" id="ingredient">
                            <option>
                                <?
                                    $query = "SELECT inID,inName FROM ingredient";
                                    $result = mysql_query($query) or die('Query failed: ' . mysql_error());
                                        while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
                                            $inid = $line["inID"];
                                            $inname = $line["inName"];
                                ?>
                                    <option value="<?=$inid?>">
                                        <?
                                            echo "$inname";
                                        ?>
                                    </option>
                              <?
                                        }
                              ?>
                        </select>
                    <strong>จำนวน</strong>            
                    <input name="txtIngredientVolume1" type="text" id="txtIngredientVolume1" size="7%" />
                    <select name="optUnitType" id="optUnitType>">
                      <option>
                                <?
                                    $query = "SELECT iuID,iuName FROM unittype";
                                    $result = mysql_query($query) or die('Query failed: ' . mysql_error());
                                        while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
                                            $iuID = $line["iuID"];
                                            $iuName = $line["iuName"];
                                ?>
                                    <option value="<?=$iuID?>">
                                        <?
                                            echo "$iuName";
                                        ?>
                                    </option>
                              <?
                                        }
                              ?>
                    </select>
                    </td> 
                  </tr>
                  <tr>
                    <th scope="row">กลุ่มอาหาร</th>
                    <td>
                          <select name="foodgroup" id="foodgroup">
                            <option>
                                <?
                                    $query = "select rgid,rgname from recipegroup";
                                    $result = mysql_query($query) or die('Query failed: ' . mysql_error());
                                        while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
                                            $rgid = $line["rgid"];
                                            $rgname = $line["rgname"];
                                ?>
                                    <option value="<?=$rgid?>">
                                        <?
                                            echo "$rgname";
                                        ?>
                                    </option>
                              <?
                                        }
                              ?>
                        </select>
                    </td> 
                  </tr>
                  <tr>
                    <th scope="row">ราคา</th>
                    <td>
                    	<input type="text" name="txtPrice" /> บาท
                    </td> 
                  </tr>
                  
         
          <tr>
            <th scope="row" align="left">เพิ่มรูปภาพ</th>
            <td colspan="5"><input type="text" name="txtPicturePath" size="70%" />
            <input type="button" name="btnBrowse" value="Browse..."/></td>
          </tr>
          <tr>
            <th scope="row" colspan="5">&nbsp;</th>
          </tr>
          <tr>
            <th scope="row" colspan="3">&nbsp;</th>
            <th colspan="2" align="left">
                <input type="submit" name="btnAddNewMenu" value="เพิ่มรายการ" />
                <input type="reset" name="btnResetContent" value="ล้างข้อมูล" />
            </th>
          </tr>
        </table>
    </form>
</body>
</html>