/*
Navicat MySQL Data Transfer

Source Server         : myConn
Source Server Version : 50051
Source Host           : localhost:3306
Source Database       : lamdeeka

Target Server Type    : MYSQL
Target Server Version : 50051
File Encoding         : 65001

Date: 2014-05-19 04:15:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `billing`
-- ----------------------------
DROP TABLE IF EXISTS `billing`;
CREATE TABLE `billing` (
  `bID` int(11) NOT NULL auto_increment,
  `uID` int(11) NOT NULL,
  `ocID` int(11) NOT NULL,
  `taxID` char(13) NOT NULL,
  `posID` char(13) NOT NULL,
  `invoiceNumber` char(14) NOT NULL,
  `billingDate` date NOT NULL,
  `billingTime` time NOT NULL,
  `ptID` int(11) NOT NULL,
  `total` double NOT NULL,
  `payment` double NOT NULL,
  `return` double NOT NULL,
  `rewards` double NOT NULL,
  `posOpenTime` time NOT NULL,
  `posCloseTime` time NOT NULL,
  PRIMARY KEY  (`bID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of billing
-- ----------------------------

-- ----------------------------
-- Table structure for `coffeephoto`
-- ----------------------------
DROP TABLE IF EXISTS `coffeephoto`;
CREATE TABLE `coffeephoto` (
  `cpID` int(11) NOT NULL auto_increment,
  `crID` int(11) NOT NULL,
  `crImage` blob,
  PRIMARY KEY  (`cpID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coffeephoto
-- ----------------------------

-- ----------------------------
-- Table structure for `coffeerecipe`
-- ----------------------------
DROP TABLE IF EXISTS `coffeerecipe`;
CREATE TABLE `coffeerecipe` (
  `crID` int(11) NOT NULL auto_increment,
  `crNameTH` varchar(30) NOT NULL,
  `crNameEN` varchar(30) NOT NULL,
  `cpID` blob,
  `rgID` int(11) NOT NULL,
  `cmPrice` double NOT NULL,
  PRIMARY KEY  (`crID`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coffeerecipe
-- ----------------------------
INSERT INTO `coffeerecipe` VALUES ('1', 'เอสเพรสโซ่', 'Espresso', null, '1', '50');
INSERT INTO `coffeerecipe` VALUES ('2', 'คาปูชิโน่', 'Capuccino', null, '1', '55');
INSERT INTO `coffeerecipe` VALUES ('3', 'ลาเต้', 'Latte', null, '1', '60');
INSERT INTO `coffeerecipe` VALUES ('4', 'ม๊อคค่า', 'Mocca', null, '1', '60');
INSERT INTO `coffeerecipe` VALUES ('5', 'โกโก้', 'Cocoa', null, '1', '45');
INSERT INTO `coffeerecipe` VALUES ('6', 'ทดลอง', 'test', null, '1', '40');
INSERT INTO `coffeerecipe` VALUES ('7', 'ลองใหม่', 'newtest', null, '1', '120');

-- ----------------------------
-- Table structure for `coffeetype`
-- ----------------------------
DROP TABLE IF EXISTS `coffeetype`;
CREATE TABLE `coffeetype` (
  `ctID` int(11) NOT NULL auto_increment,
  `ctName` varchar(15) NOT NULL,
  `ctPrice` double NOT NULL,
  PRIMARY KEY  (`ctID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coffeetype
-- ----------------------------
INSERT INTO `coffeetype` VALUES ('1', 'Hot', '0');
INSERT INTO `coffeetype` VALUES ('2', 'Cold', '10');
INSERT INTO `coffeetype` VALUES ('3', 'Frappe', '15');

-- ----------------------------
-- Table structure for `customerlogin`
-- ----------------------------
DROP TABLE IF EXISTS `customerlogin`;
CREATE TABLE `customerlogin` (
  `clID` int(11) NOT NULL auto_increment,
  `ipAddr` char(19) NOT NULL,
  `macAddr` char(20) NOT NULL,
  `loginDate` date default NULL,
  `loginTime` time default NULL,
  `logoutDate` date default NULL,
  `logoutTime` time default NULL,
  PRIMARY KEY  (`clID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of customerlogin
-- ----------------------------

-- ----------------------------
-- Table structure for `deliverstatus`
-- ----------------------------
DROP TABLE IF EXISTS `deliverstatus`;
CREATE TABLE `deliverstatus` (
  `dsID` int(11) NOT NULL auto_increment,
  `dsName` char(10) NOT NULL,
  PRIMARY KEY  (`dsID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of deliverstatus
-- ----------------------------
INSERT INTO `deliverstatus` VALUES ('1', 'Ordering');
INSERT INTO `deliverstatus` VALUES ('2', 'On-Process');
INSERT INTO `deliverstatus` VALUES ('3', 'Finishing');

-- ----------------------------
-- Table structure for `ingredient`
-- ----------------------------
DROP TABLE IF EXISTS `ingredient`;
CREATE TABLE `ingredient` (
  `inID` int(11) NOT NULL auto_increment,
  `inName` varchar(50) NOT NULL,
  `addDate` date NOT NULL,
  `addTime` time NOT NULL,
  `editDate` date default NULL,
  `editTime` time default NULL,
  PRIMARY KEY  (`inID`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ingredient
-- ----------------------------
INSERT INTO `ingredient` VALUES ('1', 'กาแฟ', '2014-04-15', '16:44:00', '0000-00-00', '00:00:00');
INSERT INTO `ingredient` VALUES ('2', 'นมมิก', '2014-04-15', '16:45:04', '0000-00-00', '00:00:00');
INSERT INTO `ingredient` VALUES ('3', 'นมสด', '2014-04-15', '16:45:56', '0000-00-00', '00:00:00');
INSERT INTO `ingredient` VALUES ('4', 'นมข้นจืด', '2014-04-15', '16:46:22', '0000-00-00', '00:00:00');
INSERT INTO `ingredient` VALUES ('9', 'ลองอีก', '2014-05-19', '05:07:45', null, null);
INSERT INTO `ingredient` VALUES ('10', 'test again', '2014-05-19', '05:07:55', null, null);

-- ----------------------------
-- Table structure for `ingredientunit`
-- ----------------------------
DROP TABLE IF EXISTS `ingredientunit`;
CREATE TABLE `ingredientunit` (
  `iuID` int(11) NOT NULL auto_increment,
  `inID` int(11) NOT NULL,
  `utID` int(11) NOT NULL,
  `volume` double NOT NULL,
  PRIMARY KEY  (`iuID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ingredientunit
-- ----------------------------
INSERT INTO `ingredientunit` VALUES ('1', '1', '1', '2');
INSERT INTO `ingredientunit` VALUES ('2', '2', '1', '2');
INSERT INTO `ingredientunit` VALUES ('3', '3', '1', '2');

-- ----------------------------
-- Table structure for `member`
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `mID` int(11) NOT NULL auto_increment,
  `mName` varchar(20) NOT NULL,
  `mLname` varchar(20) NOT NULL,
  `mtID` int(11) NOT NULL,
  `mPicture` blob,
  `mMail` char(20) default NULL,
  `mMobile` char(10) NOT NULL,
  `mRegDate` date NOT NULL,
  `mEarnPoint` double NOT NULL,
  PRIMARY KEY  (`mID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member
-- ----------------------------
INSERT INTO `member` VALUES ('1', 'Chatnantapon', 'Ratnaratorn', '1', null, 'first.cmu.se@gmail.c', '0843062529', '2014-04-15', '0');

-- ----------------------------
-- Table structure for `memberreward`
-- ----------------------------
DROP TABLE IF EXISTS `memberreward`;
CREATE TABLE `memberreward` (
  `mrID` int(11) NOT NULL auto_increment,
  `mID` int(11) NOT NULL,
  `mRewarded` double NOT NULL,
  PRIMARY KEY  (`mrID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of memberreward
-- ----------------------------

-- ----------------------------
-- Table structure for `membertype`
-- ----------------------------
DROP TABLE IF EXISTS `membertype`;
CREATE TABLE `membertype` (
  `mtID` int(11) NOT NULL auto_increment,
  `mtName` char(20) NOT NULL,
  PRIMARY KEY  (`mtID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of membertype
-- ----------------------------
INSERT INTO `membertype` VALUES ('1', 'Normal');
INSERT INTO `membertype` VALUES ('2', 'Premier');
INSERT INTO `membertype` VALUES ('3', 'Executive');

-- ----------------------------
-- Table structure for `ordercoffee`
-- ----------------------------
DROP TABLE IF EXISTS `ordercoffee`;
CREATE TABLE `ordercoffee` (
  `ocID` int(11) NOT NULL auto_increment,
  `crID` int(11) NOT NULL,
  `ctID` int(11) NOT NULL,
  `dsID` int(11) NOT NULL,
  `orderNumber` int(11) NOT NULL,
  `tnID` int(11) NOT NULL,
  `mID` int(11) NOT NULL,
  `uID` int(11) NOT NULL,
  `orderDate` date NOT NULL,
  `orderTime` time NOT NULL,
  `total` double(11,0) NOT NULL,
  PRIMARY KEY  (`ocID`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ordercoffee
-- ----------------------------
INSERT INTO `ordercoffee` VALUES ('1', '1', '2', '1', '0', '3', '999', '1', '0000-00-00', '00:00:00', '60');
INSERT INTO `ordercoffee` VALUES ('2', '1', '2', '1', '0', '3', '999', '1', '2014-04-15', '00:00:00', '60');
INSERT INTO `ordercoffee` VALUES ('3', '1', '2', '1', '0', '3', '999', '1', '2014-04-15', '23:37:37', '60');
INSERT INTO `ordercoffee` VALUES ('4', '1', '2', '1', '1', '3', '999', '1', '2014-04-15', '23:41:00', '60');
INSERT INTO `ordercoffee` VALUES ('5', '1', '2', '1', '1', '3', '999', '1', '2014-04-15', '23:41:09', '60');
INSERT INTO `ordercoffee` VALUES ('6', '1', '2', '1', '1', '3', '999', '1', '2014-04-15', '23:41:28', '60');
INSERT INTO `ordercoffee` VALUES ('7', '3', '3', '1', '1', '8', '999', '1', '2014-04-15', '23:41:39', '75');
INSERT INTO `ordercoffee` VALUES ('8', '3', '3', '1', '1', '8', '999', '1', '2014-04-15', '23:42:08', '75');
INSERT INTO `ordercoffee` VALUES ('9', '3', '3', '1', '2', '8', '999', '1', '2014-04-15', '23:42:20', '75');
INSERT INTO `ordercoffee` VALUES ('10', '3', '3', '1', '3', '8', '999', '1', '2014-04-15', '23:42:24', '75');
INSERT INTO `ordercoffee` VALUES ('11', '3', '3', '1', '4', '8', '999', '1', '2014-04-15', '23:42:27', '75');
INSERT INTO `ordercoffee` VALUES ('12', '3', '3', '1', '5', '8', '999', '1', '2014-04-15', '23:42:27', '75');
INSERT INTO `ordercoffee` VALUES ('13', '3', '3', '1', '6', '8', '999', '1', '2014-04-15', '23:42:27', '75');
INSERT INTO `ordercoffee` VALUES ('14', '3', '3', '1', '7', '8', '999', '1', '2014-04-15', '23:42:28', '75');
INSERT INTO `ordercoffee` VALUES ('15', '3', '3', '1', '8', '8', '999', '1', '2014-04-15', '23:42:28', '75');
INSERT INTO `ordercoffee` VALUES ('16', '3', '3', '1', '9', '8', '999', '1', '2014-04-15', '23:42:28', '75');
INSERT INTO `ordercoffee` VALUES ('17', '3', '3', '1', '10', '8', '999', '1', '2014-04-15', '23:42:28', '75');
INSERT INTO `ordercoffee` VALUES ('18', '3', '3', '1', '11', '8', '999', '1', '2014-04-15', '23:42:28', '75');
INSERT INTO `ordercoffee` VALUES ('19', '3', '3', '1', '12', '8', '999', '1', '2014-04-15', '23:42:29', '75');
INSERT INTO `ordercoffee` VALUES ('20', '3', '3', '1', '13', '8', '999', '1', '2014-04-15', '23:42:29', '75');
INSERT INTO `ordercoffee` VALUES ('21', '3', '3', '1', '14', '8', '999', '1', '2014-04-15', '23:42:29', '75');
INSERT INTO `ordercoffee` VALUES ('22', '3', '3', '1', '15', '8', '999', '1', '2014-04-15', '23:42:29', '75');
INSERT INTO `ordercoffee` VALUES ('23', '3', '3', '1', '16', '8', '999', '1', '2014-04-15', '23:42:29', '75');
INSERT INTO `ordercoffee` VALUES ('24', '3', '3', '1', '17', '8', '999', '1', '2014-04-15', '23:42:30', '75');
INSERT INTO `ordercoffee` VALUES ('25', '3', '3', '1', '18', '8', '999', '1', '2014-04-15', '23:42:30', '75');
INSERT INTO `ordercoffee` VALUES ('26', '3', '3', '1', '19', '8', '999', '1', '2014-04-15', '23:42:30', '75');
INSERT INTO `ordercoffee` VALUES ('27', '3', '3', '1', '20', '8', '999', '1', '2014-04-15', '23:42:30', '75');
INSERT INTO `ordercoffee` VALUES ('28', '3', '3', '1', '21', '8', '999', '1', '2014-04-15', '23:42:31', '75');
INSERT INTO `ordercoffee` VALUES ('29', '6', '3', '1', '22', '8', '999', '1', '2014-05-19', '04:48:25', '55');
INSERT INTO `ordercoffee` VALUES ('30', '1', '3', '1', '23', '10', '999', '1', '2014-05-19', '05:00:02', '65');

-- ----------------------------
-- Table structure for `paymenttype`
-- ----------------------------
DROP TABLE IF EXISTS `paymenttype`;
CREATE TABLE `paymenttype` (
  `ptID` int(11) NOT NULL auto_increment,
  `ptName` char(20) NOT NULL,
  PRIMARY KEY  (`ptID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of paymenttype
-- ----------------------------
INSERT INTO `paymenttype` VALUES ('1', 'Cash');
INSERT INTO `paymenttype` VALUES ('2', 'Credit');
INSERT INTO `paymenttype` VALUES ('3', 'Redeem');

-- ----------------------------
-- Table structure for `recipegroup`
-- ----------------------------
DROP TABLE IF EXISTS `recipegroup`;
CREATE TABLE `recipegroup` (
  `rgID` int(11) NOT NULL auto_increment,
  `rgName` varchar(15) NOT NULL,
  PRIMARY KEY  (`rgID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of recipegroup
-- ----------------------------
INSERT INTO `recipegroup` VALUES ('1', 'Coffee');
INSERT INTO `recipegroup` VALUES ('2', 'Cake');
INSERT INTO `recipegroup` VALUES ('3', 'Food');
INSERT INTO `recipegroup` VALUES ('4', 'Beer');

-- ----------------------------
-- Table structure for `tablenum`
-- ----------------------------
DROP TABLE IF EXISTS `tablenum`;
CREATE TABLE `tablenum` (
  `tnID` int(11) NOT NULL auto_increment,
  `tnNumber` char(3) NOT NULL,
  `tnZone` varchar(20) NOT NULL,
  PRIMARY KEY  (`tnID`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tablenum
-- ----------------------------
INSERT INTO `tablenum` VALUES ('1', '1', 'ด้านใน');
INSERT INTO `tablenum` VALUES ('2', '2', 'ด้านใน');
INSERT INTO `tablenum` VALUES ('3', '3', 'ด้านใน');
INSERT INTO `tablenum` VALUES ('4', '4', 'ด้านใน');
INSERT INTO `tablenum` VALUES ('5', '5', 'ด้านใน');
INSERT INTO `tablenum` VALUES ('6', '1', 'ด้านนอก');
INSERT INTO `tablenum` VALUES ('7', '2', 'ด้านนอก');
INSERT INTO `tablenum` VALUES ('8', '3', 'ด้านนอก');
INSERT INTO `tablenum` VALUES ('9', '4', 'ด้านนอก');
INSERT INTO `tablenum` VALUES ('10', '5', 'ด้านนอก');
INSERT INTO `tablenum` VALUES ('11', '6', 'ด้านนอก');

-- ----------------------------
-- Table structure for `unittype`
-- ----------------------------
DROP TABLE IF EXISTS `unittype`;
CREATE TABLE `unittype` (
  `iuID` int(11) NOT NULL auto_increment,
  `iuName` varchar(30) NOT NULL,
  PRIMARY KEY  (`iuID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of unittype
-- ----------------------------
INSERT INTO `unittype` VALUES ('1', 'ออนซ์');
INSERT INTO `unittype` VALUES ('2', 'ปั๊ม');
INSERT INTO `unittype` VALUES ('3', 'ช้อนโต๊ะ');
INSERT INTO `unittype` VALUES ('4', 'ชอต');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `uID` int(11) NOT NULL auto_increment,
  `uName` varchar(20) NOT NULL,
  `uLname` varchar(20) NOT NULL,
  `username` char(20) NOT NULL,
  `password` char(20) NOT NULL,
  `conPass` char(20) NOT NULL,
  `uMail` char(20) NOT NULL,
  `uMobile` char(10) NOT NULL,
  `uPicture` blob NOT NULL,
  `uRegDate` date NOT NULL,
  `utID` int(11) NOT NULL,
  PRIMARY KEY  (`uID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'Prin', 'Kammasitt', 'zaat', 'zaat', 'zaat', 'mp.prarin@live.com', '0882524210', '', '2014-04-14', '3');
INSERT INTO `user` VALUES ('2', 'Tavin', 'Kuanstaporn', 'moxy', 'moxy', 'moxy', 'Mmooxxyy@gmail.com', '0869795237', '', '2014-04-14', '3');
INSERT INTO `user` VALUES ('3', 'Teerapop', 'Samutranuparp', 'tospace', 'tospace', 'tospace', 'tomori.top@gmail.com', '0922542453', '', '2014-04-14', '3');
INSERT INTO `user` VALUES ('4', 'แจ๋ว', 'เยี่ยมยอด', 'jj', 'yy', 'yy', 'aaa@df.com', '0812345689', '', '2014-04-15', '1');

-- ----------------------------
-- Table structure for `useredittype`
-- ----------------------------
DROP TABLE IF EXISTS `useredittype`;
CREATE TABLE `useredittype` (
  `uEdittypeID` int(11) NOT NULL auto_increment,
  `uEdittypeName` int(11) NOT NULL,
  PRIMARY KEY  (`uEdittypeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of useredittype
-- ----------------------------

-- ----------------------------
-- Table structure for `userlogs`
-- ----------------------------
DROP TABLE IF EXISTS `userlogs`;
CREATE TABLE `userlogs` (
  `ulID` int(11) NOT NULL auto_increment,
  `uID` int(11) NOT NULL,
  `uEdittypeID` int(11) NOT NULL,
  `Description` text,
  PRIMARY KEY  (`ulID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userlogs
-- ----------------------------

-- ----------------------------
-- Table structure for `usertype`
-- ----------------------------
DROP TABLE IF EXISTS `usertype`;
CREATE TABLE `usertype` (
  `utID` int(11) NOT NULL auto_increment,
  `utName` varchar(20) NOT NULL,
  PRIMARY KEY  (`utID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usertype
-- ----------------------------
INSERT INTO `usertype` VALUES ('1', 'Employee');
INSERT INTO `usertype` VALUES ('2', 'Member');
INSERT INTO `usertype` VALUES ('3', 'Admin');
INSERT INTO `usertype` VALUES ('4', 'Owner');
