<?
	include("connDB.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>สั่งกาแฟ</title>
</head>

<body>
    <form id="form1" name="form1" method="post" action="orderCoffee_DB.php">
      <table border="1">
        <tr align="left">
          <th scope="row" colspan="2" align="center">สั่งกาแฟ</th>
        </tr>
        <tr align="left">
          <th scope="row" colspan="2">&nbsp;</th>
        </tr>
        
        
        <!-- เลือกเมนูที่ต้องการสั่ง -->
        <tr align="left">
          <th scope="row">เลือกเมนู</th>
          <td>
          	<select name="coffeeMenu" id="coffeeMenu">
            	<?
					$cnt;
					
                	$query = "select crID,crNameTH,crNameEN,cmPrice from coffeerecipe";
					$result = mysql_query($query) or die('Query failed: ' . mysql_error());
						while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
							$crID = $line["crID"];
							$cnTH = $line["crNameTH"];
							$cnEN = $line["crNameEN"];
							$cPrice = $line["cmPrice"];
				?>
                <option value="<?=$crID . "," . $cPrice?>">
					<?
                    	echo "$cnTH ($cnEN) | $cPrice บาท";
					?>
                </option>
                <?
						}
				?>
            </select>
          </td>
        </tr>
        
        <tr align="left">
          <th scope="row">เลือกประเภท</th>
          <td>
              <select name="coffeeType" id="coffeeType">
                    <?
                        $cnt;
                        
                        $query = "select ctID,ctName,ctPrice from coffeetype";
                        $result = mysql_query($query) or die('Query failed: ' . mysql_error());
                            while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
                                $ctID = $line["ctID"];
                                $ctName = $line["ctName"];
                                $ctPrice = $line["ctPrice"];
                    ?>
                    <option value="<?=$ctID . "," . $ctPrice?>">
                        <?
                            echo "$ctName (+$ctPrice บาท)";
                        ?>
                    </option>
                    <?
                            }
                    ?>
              </select>
          </td>
        </tr>
        
        <tr align="left">
          <th scope="row">หมายเลขโต๊ะ</th>
          <td>
              <select name="tableNumber" id="tableNumber">
                    <?
                        $cnt;
                        
                        $query = "SELECT tnID,tnNumber,tnZone from tablenum";
                        $result = mysql_query($query) or die('Query failed: ' . mysql_error());
                            while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
                                $tnID = $line["tnID"];
                                $tnNumber = $line["tnNumber"];
                                $tnZone = $line["tnZone"];
                    ?>
                    <option value="<?=$tnID?>">
                        <?
                            echo "$tnNumber ($tnZone)";
                        ?>
                    </option>
                    <?
                            }
                    ?>
        		</select>
          </td>
        </tr>
        
        <script type="text/javascript">
			function computePrice(){
			  var cP = document.getElementById("coffeeMenu");
			  var cP_value = cP.options[cP.selectedIndex].value;
			  var cPrice = cP_value.split(',')
			  var cp_double = parseFloat(cPrice[1]);
			  
			  var cT = document.getElementById("coffeeType");
			  var cT_value = cT.options[cT.selectedIndex].value;
			  var cTPrice = cT_value.split(',');
			  var cTPrice_double = parseFloat(cTPrice[1]);
			  
			  var total = cp_double + cTPrice_double;
			  
			  document.cookie="cP_value="+cp_double;
			  document.cookie="cT_value="+cTPrice_double;
			  document.cookie="total_value="+total;
			  
			  //alert("cPrice= "+cp_double +"\ncTPrice= "+cTPrice_double+"\nTotal: "+ total);
			}
		</script>
        
        <tr align="right">
          <th scope="row"></th>
          <td>
          	<input type="submit" value="สั่งกาแฟ" onclick="return computePrice();" />
            <input type="reset"  value="เริ่มต้นใหม่"/>
          </td>
        </tr>
        
      </table>
    </form>
</body>
</html>